package com.example.employees.service;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class NumberService {

    public List<Integer> findPrimeNumbers(int toNumber) {

        if(toNumber < 2) {
            return null;
        }

        IntStream stream = IntStream.range(2, toNumber+1);

        return stream.filter(NumberService::isPrime)
                .boxed()
                .collect(Collectors.toList());
    }

    public static boolean isPrime(int i)
    {
        IntPredicate isDivisible = index -> i % index == 0;
        return i > 1 && IntStream.range(2, i).noneMatch(isDivisible);
    }
}
