package com.example.employees.controller;

import com.example.employees.service.NumberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http:localhost:8080", maxAge = 3600)
@RestController
@RequestMapping(value = "/primes")
public class PrimeController {

    private static final Logger LOG = LoggerFactory.getLogger(PrimeController.class);

    private final NumberService numberService;

    public PrimeController(NumberService numberService) {
        this.numberService = numberService;
    }

    @GetMapping(value = "/{toNumber}")
    public ResponseEntity<List<Integer>> findPrimes(@PathVariable(value = "toNumber") int toNumber) {
        if (toNumber < 2) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<Integer> primeNumbers = numberService.findPrimeNumbers(toNumber);
        return new ResponseEntity<>(primeNumbers, HttpStatus.OK);
    }
}
