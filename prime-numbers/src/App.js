import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import React from "react";
import axios from "axios";
import {Form} from "react-bootstrap";

class App extends React.Component {

    state = {
        numbers: [],
        isTooLarge : false
    }

    getNumbers = (number) => {
        axios.get(`http://localhost:8080/primes/${number}`)
            .then(res => {
                const numbers = res.data;
                this.setState({numbers, isTooLarge:false})
            })
    }

    handleNumberChange = (e) => {
        const inputNumber = e.target.value;
        if (inputNumber < 2147483647) {
            if (inputNumber > 0) {
                this.getNumbers(inputNumber)
            } else if (this.state.numbers.length > 0) {
                this.setState({numbers: []})
            }
        } else {
            this.setState({isTooLarge:true, numbers:[]})
        }
    }

    render() {
        return (
            <div className="App">
                <div className="container align-items-center position-relative">
                    {<Form className="d-flex justify-content-center">
                        <Form.Group className="mb-1" controlId="formBasicEmail">
                            <Form.Label>Enter Number</Form.Label>
                            <Form.Control type="number" onChange={this.handleNumberChange} />
                            <Form.Text className="text-muted">
                                No number larger than 2147483647
                            </Form.Text>
                        </Form.Group>
                    </Form>}
                    {this.state.isTooLarge && <div>Error Number too large</div>}
                    <div className="row w-100">
                    {this.state.numbers.map(num => {
                            return <NumberContainer
                                number={num}
                            />
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

const InputContainer = ({handleChange}) => <input className="input-bar" onChange={handleChange} type="number"
                                                  placeholder="Find prime numbers up to this number"/>
const NumberContainer = ({number}) => <div className="col-1">{number}</div>

export default App;
